/**
 * Different types in Java:
 * class
 * enum
 * primitives
 * interface
 * 
 * class is a combination of data and methods
 * interface only defines methods that its subclasses will have
 * everything in an interface is public by definition
 * 
 * 
 * @author unouser
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
