import java.io.Serializable;

/**
 * Comment workflow in java:
 * 
 * Create an interface which will let us program in general
 * Create an abstract class which will let us code shared functionality
 * Create concrete classes
 * 
 * Abstract:
 * -cannot be instantiated
 * -All concrete classes that inherit from abstract class must implement all abstract methods
 * -Abstract subclassess may or may not implement the abstract methods
 * 
 * Implements means I'm inheriting from an interface. You can implement as many interfaces as you want
 * 
 * Because SubaryLegacy nor ACar have a toString, main calls toString from Object class
 * @author unouser
 *
 */
public abstract class ACar implements ICar, Serializable{
	
	private String make;
	
	private String model;
	
	private int year;
	
	private int mileage;
	
	/**
	 * The constructor for an abstract car
	 * @param inMake The make of the car
	 * @param inModel The model of the car
	 * @param inYear The year of the car
	 */
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		return getMake() + " " + getModel();
	}
	
	
}
